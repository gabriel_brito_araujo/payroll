using Moq;
using NUnit.Framework;
using Payroll.Domain.PostalCodeDomain.Entities;
using Payroll.Domain.PostalCodeDomain.Repositories;
using Payroll.Domain.RateRangeDomain.Entities;
using Payroll.Domain.RateRangeDomain.Repositories;
using Payroll.Domain.TaxDomain.Entities;
using Payroll.Domain.TaxDomain.Enums;
using Payroll.Domain.TaxDomain.Repository;
using Payroll.Services;
using System.Threading.Tasks;

namespace UnitTests.TaxServiceTests
{
    public class TaxServiceTests
    {
        private TaxService _taxService;
        private Mock<IPostalCodeRepository> _postalCodeRepository;
        private Mock<IRateRangeRepository> _rateRangeRepository;
        private Mock<ITaxRepository> _taxRepository;

        [SetUp]
        public void Setup()
        {
            _postalCodeRepository = new Mock<IPostalCodeRepository>();
            _rateRangeRepository = new Mock<IRateRangeRepository>();
            _taxRepository = new Mock<ITaxRepository>();
            _taxService = new TaxService(_postalCodeRepository.Object,
                _rateRangeRepository.Object, _taxRepository.Object);
        }

        [Test]
        public async Task PostalCodeNotInformed_Fail()
        {
            var response = await _taxService.GetTax("", 21321.2);

            Assert.IsFalse(response.IsSuccess);
            Assert.AreEqual("Postal Code is required", response.Message);
        }

        [Test]
        public async Task PostalCodeNotFound_Fail()
        {
            _postalCodeRepository.Setup(x => x.Get(It.IsAny<string>()))
                .ReturnsAsync((PostalCode)null);

            var response = await _taxService.GetTax("2132132", 21321.2);

            Assert.IsFalse(response.IsSuccess);
            Assert.AreEqual("Postal Code was not found", response.Message);
        }

        [Test]
        public async Task TaxInvalidState_Fail()
        {
            _postalCodeRepository.Setup(x => x.Get(It.IsAny<string>()))
                .ReturnsAsync(PostalCode.Load(1, "122", TaxType.Flat));

            var response = await _taxService.GetTax("2132132", 0);

            Assert.IsFalse(response.IsSuccess);
            Assert.AreEqual("Amount must be informed.", response.Message);
        }

        [Test]
        public async Task FlatTax_Success()
        {
            var amount = 344345.98;
            var code = "122";

            _postalCodeRepository.Setup(x => x.Get(It.IsAny<string>()))
                .ReturnsAsync(PostalCode.Load(1, code, TaxType.Flat));

            _rateRangeRepository.Setup(x => x.Get(It.IsAny<double>(), It.IsAny<TaxType>()))
                .ReturnsAsync(new[]
                {
                    RateRange.Load(7,5, 10000,199999,TaxType.Flat),
                    RateRange.Load(8,17.5,200000,double.MaxValue,TaxType.Flat)
                });

            var response = await _taxService.GetTax(code, amount);

            var expected = 60260.546499999989;
            var result = (double)response.Data;

            Assert.IsTrue(response.IsSuccess);
            Assert.AreEqual(expected, result);


            _taxRepository.Verify(x => x.Store(It.IsAny<FlatTax>()), Times.Once);
        }

        [Test]
        public async Task ProgressiveTax_Success()
        {
            var amount = 344345.98;
            var code = "A122";

            _postalCodeRepository.Setup(x => x.Get(It.IsAny<string>()))
                .ReturnsAsync(PostalCode.Load(1, code, TaxType.Progressive));

            _rateRangeRepository.Setup(x => x.Get(It.IsAny<double>(), It.IsAny<TaxType>()))
                .ReturnsAsync(new[]
                {
                    RateRange.Load(1,10,0,8350, TaxType.Progressive),
                    RateRange.Load(2,-15,8351,33950,TaxType.Progressive),
                    RateRange.Load(3,25,33951,82250,TaxType.Progressive),
                    RateRange.Load(4,28,82251,171550,TaxType.Progressive),
                    RateRange.Load(5,33,171551,372950,TaxType.Progressive),
                    RateRange.Load(6,35,372951,double.MaxValue,TaxType.Progressive)
                });

            var response = await _taxService.GetTax(code, amount);

            var expected = 80260.1734;
            var result = (double)response.Data;

            Assert.IsTrue(response.IsSuccess);
            Assert.AreEqual(expected, result);

            _taxRepository.Verify(x => x.Store(It.IsAny<ProgressiveTax>()), Times.Once);

        }
    }
}