﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Payroll.Domain.BaseStructure.Implementations;

namespace Payroll.Mvc.Controllers
{
    public class BaseController: Controller
    {
        public override ViewResult View(object model)
        {
            if (model is ResponseMessage responseMessage)
            {
                if (!responseMessage.IsSuccess)
                {
                    ModelState.AddModelError("Errors", responseMessage.Message);
                    return base.View();
                }

                return base.View(responseMessage.Data);
            }

            return base.View(model);
        }

        public override ViewResult View(string viewName, object model)
        {
            if (model is ResponseMessage responseMessage)
            {
                if (!responseMessage.IsSuccess)
                {
                    ModelState.AddModelError("Errors", responseMessage.Message);
                    return base.View();
                }

                return base.View(viewName, responseMessage.Data);
            }

            return base.View(viewName, model);
        }

    }
}
