﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Payroll.Domain.PostalCodeDomain.Repositories;
using Payroll.Domain.TaxDomain.Services;
using Payroll.Domain.TaxDomain.ViewModels;
using Payroll.Mvc.Models;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Payroll.Mvc.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ITaxService _taxService;
        private readonly IPostalCodeRepository _postalCodeRepository;

        public HomeController(ITaxService taxService,
            IPostalCodeRepository postalCodeRepository)
        {
            _taxService = taxService;
            _postalCodeRepository = postalCodeRepository;
        }

        public async Task<IActionResult> Index()
        {
            var postalCodes = await _postalCodeRepository.Get();

            ViewBag.PostalCodes = postalCodes.Select(pc =>
                new SelectListItem()
                {
                    Text = pc.Code,
                    Value = pc.Code
                });

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(CalculatorVm calculatorVm)
        {
            var tax = await _taxService.GetTax(calculatorVm.PostalCode, calculatorVm.Amount);

            return View("Result", new ResultVm { Tax = tax.Data });
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
