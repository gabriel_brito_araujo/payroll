﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Payroll.Domain.PostalCodeDomain.Repositories;
using Payroll.Domain.RateRangeDomain.Repositories;
using Payroll.Domain.TaxDomain.Repository;
using Payroll.Domain.TaxDomain.Services;
using Payroll.Infrastructure.Database;
using Payroll.Infrastructure.Database.Repositorioes;
using Payroll.Services;

namespace Payroll.Mvc.Extensions
{
    public static class DependencyInjectionExtension
    {
        public static void AddDependenciesInjection(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("PayrollDb");

            services.AddDbContext<PayrollDataContext>(options => options.UseSqlServer(connectionString));

            services.AddOptions();

            services.AddScoped<IPostalCodeRepository, PostalCodeRepository>();
            services.AddScoped<IRateRangeRepository, RateRangeRepository>();
            services.AddScoped<ITaxRepository, TaxRepository>();

            services.AddScoped<ITaxService, TaxService>();
        }
    }
}
