﻿using System.ComponentModel.DataAnnotations;

namespace Payroll.Mvc.Models
{
    public class CalculatorVm
    {
        [Required]
        public string PostalCode { get; set; }
        [Required]
        public double Amount { get; set; }
    }
}
