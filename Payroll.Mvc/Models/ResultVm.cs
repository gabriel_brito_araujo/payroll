﻿using System.ComponentModel.DataAnnotations;

namespace Payroll.Mvc.Models
{
    public class ResultVm
    {
        [DisplayFormat(DataFormatString = "{0:0.0000}")]
        public object Tax { get; set; }
    }
}
