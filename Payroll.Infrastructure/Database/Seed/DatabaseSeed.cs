﻿using Microsoft.EntityFrameworkCore;
using Payroll.Domain.PostalCodeDomain.Entities;
using Payroll.Domain.RateRangeDomain.Entities;
using Payroll.Domain.TaxDomain.Enums;

namespace Payroll.Infrastructure.Database.Seed
{
    public static class DatabaseSeed
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PostalCode>()
                .HasData(new PostalCode[4]
                {
                    PostalCode.Load(1,"7441", TaxType.Progressive),
                    PostalCode.Load(2,"A100", TaxType.Flat),
                    PostalCode.Load(3,"7000", TaxType.Flat),
                    PostalCode.Load(4,"1000", TaxType.Progressive)
                });

            modelBuilder.Entity<RateRange>()
                .HasData(new RateRange[8]
                {
                    RateRange.Load(1,10,0,8350, TaxType.Progressive),
                    RateRange.Load(2,-15,8351,33950,TaxType.Progressive),
                    RateRange.Load(3,25,33951,82250,TaxType.Progressive),
                    RateRange.Load(4,28,82251,171550,TaxType.Progressive),
                    RateRange.Load(5,33,171551,372950,TaxType.Progressive),
                    RateRange.Load(6,35,372951,double.MaxValue,TaxType.Progressive),
                    RateRange.Load(7,5, 10000,199999,TaxType.Flat),
                    RateRange.Load(8,17.5,200000,double.MaxValue,TaxType.Flat)
                });
        }
    }
}
