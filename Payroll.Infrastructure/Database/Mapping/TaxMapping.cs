﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Payroll.Domain.PostalCodeDomain.Entities;
using Payroll.Domain.TaxDomain.Entities;

namespace Payroll.Infrastructure.Database.Mapping
{
    public class TaxMapping : IEntityTypeConfiguration<Tax>
    {
        public void Configure(EntityTypeBuilder<Tax> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .ValueGeneratedOnAdd();


            builder.Ignore(x => x.IsNew);
            builder.Ignore(x => x.Validator);
        }
    }

    public class ProgressiveTaxMapping : IEntityTypeConfiguration<ProgressiveTax>
    {
        public void Configure(EntityTypeBuilder<ProgressiveTax> builder)
        {

        }
    }

    public class FlatTaxMapping : IEntityTypeConfiguration<FlatTax>
    {
        public void Configure(EntityTypeBuilder<FlatTax> builder)
        {
        }
    }
}
