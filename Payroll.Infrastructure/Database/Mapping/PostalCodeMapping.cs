﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Payroll.Domain.PostalCodeDomain.Entities;

namespace Payroll.Infrastructure.Database.Mapping
{
    public class PostalCodeMapping: IEntityTypeConfiguration<PostalCode>
    {
        public void Configure(EntityTypeBuilder<PostalCode> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .IsRequired()
                .ValueGeneratedNever();

            builder.Property(c => c.TaxType)
                .HasConversion<int>();

            builder.Ignore(x => x.IsNew);
            builder.Ignore(x => x.Validator);
        }
    }
}
