﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Payroll.Domain.RateRangeDomain.Entities;

namespace Payroll.Infrastructure.Database.Mapping
{
    public class RateRangeMapping : IEntityTypeConfiguration<RateRange>
    {
        public void Configure(EntityTypeBuilder<RateRange> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .IsRequired()
                .ValueGeneratedNever();

            builder.Property(c => c.TaxType)
                .HasConversion<int>();

            builder.Ignore(x => x.IsNew);
            builder.Ignore(x => x.Validator);
        }
    }
}
