﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Payroll.Domain.PostalCodeDomain.Entities;
using Payroll.Domain.PostalCodeDomain.Repositories;

namespace Payroll.Infrastructure.Database.Repositorioes
{
    public class PostalCodeRepository: IPostalCodeRepository
    {
        private readonly PayrollDataContext _payrollDataContext;

        public PostalCodeRepository(PayrollDataContext payrollDataContext)
        {
            _payrollDataContext = payrollDataContext;
        }

        public async Task<IEnumerable<PostalCode>> Get()
        {
            return await _payrollDataContext.PostalCodes
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<PostalCode> Get(string postalCode)
        {
            return await _payrollDataContext.PostalCodes
               .AsNoTracking()
               .FirstOrDefaultAsync(x => x.Code == postalCode);
        }
    }
}
