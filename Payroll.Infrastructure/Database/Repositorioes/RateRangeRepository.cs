﻿using Microsoft.EntityFrameworkCore;
using Payroll.Domain.RateRangeDomain.Entities;
using Payroll.Domain.RateRangeDomain.Repositories;
using Payroll.Domain.TaxDomain.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Payroll.Infrastructure.Database.Repositorioes
{
    public class RateRangeRepository : IRateRangeRepository
    {
        private readonly PayrollDataContext _payrollDataContext;

        public RateRangeRepository(PayrollDataContext payrollDataContext)
        {
            _payrollDataContext = payrollDataContext;
        }

        public async Task<IEnumerable<RateRange>> Get(double amount, TaxType taxType)
        {
            return await _payrollDataContext.RateRanges
                .AsNoTracking()
                .Where(x => x.To <= amount && x.TaxType == taxType)
                .ToListAsync();
        }
    }
}
