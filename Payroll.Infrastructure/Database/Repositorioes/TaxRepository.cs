﻿using System.Threading.Tasks;
using Payroll.Domain.TaxDomain.Entities;
using Payroll.Domain.TaxDomain.Repository;

namespace Payroll.Infrastructure.Database.Repositorioes
{
    public class TaxRepository : ITaxRepository
    {
        private readonly PayrollDataContext _payrollDataContext;

        public TaxRepository(PayrollDataContext payrollDataContext)
        {
            _payrollDataContext = payrollDataContext;
        }

        public async Task Store(Tax tax)
        {
            if (tax.IsNew)
            {
                await _payrollDataContext.Taxes.AddAsync(tax);
            }
            else
            {
                _payrollDataContext.Taxes.Attach(tax);
            }

            await _payrollDataContext.SaveChangesAsync();
        }
    }
}
