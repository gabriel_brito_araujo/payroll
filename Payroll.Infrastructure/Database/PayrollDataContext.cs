﻿using Microsoft.EntityFrameworkCore;
using Payroll.Domain.PostalCodeDomain.Entities;
using Payroll.Domain.RateRangeDomain.Entities;
using Payroll.Domain.TaxDomain.Entities;
using Payroll.Infrastructure.Database.Mapping;
using Payroll.Infrastructure.Database.Seed;

namespace Payroll.Infrastructure.Database
{
    public class PayrollDataContext : DbContext
    {
        public PayrollDataContext(DbContextOptions<PayrollDataContext> options)
        : base(options)
        {
        }

        public DbSet<RateRange> RateRanges { get; set; }
        public DbSet<PostalCode> PostalCodes { get; set; }
        public DbSet<Tax> Taxes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.ApplyConfiguration(new PostalCodeMapping());
            modelBuilder.ApplyConfiguration(new RateRangeMapping());
            modelBuilder.ApplyConfiguration(new TaxMapping());
            modelBuilder.ApplyConfiguration(new FlatTaxMapping());
            modelBuilder.ApplyConfiguration(new ProgressiveTaxMapping());

            modelBuilder.Seed();


            base.OnModelCreating(modelBuilder);
        }
    }
}
