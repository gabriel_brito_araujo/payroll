﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Payroll.Infrastructure.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PostalCodes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    TaxType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PostalCodes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RateRanges",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Rate = table.Column<double>(nullable: false),
                    From = table.Column<double>(nullable: false),
                    To = table.Column<double>(nullable: false),
                    TaxType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RateRanges", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Taxes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<double>(nullable: false),
                    TaxAmount = table.Column<double>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Taxes", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "PostalCodes",
                columns: new[] { "Id", "Code", "TaxType" },
                values: new object[,]
                {
                    { 1, "7441", 1 },
                    { 2, "A100", 2 },
                    { 3, "7000", 2 },
                    { 4, "1000", 1 }
                });

            migrationBuilder.InsertData(
                table: "RateRanges",
                columns: new[] { "Id", "From", "Rate", "TaxType", "To" },
                values: new object[,]
                {
                    { 1, 0.0, 10.0, 1, 8350.0 },
                    { 2, 8351.0, -15.0, 1, 33950.0 },
                    { 3, 33951.0, 25.0, 1, 82250.0 },
                    { 4, 82251.0, 28.0, 1, 171550.0 },
                    { 5, 171551.0, 33.0, 1, 372950.0 },
                    { 6, 372951.0, 35.0, 1, 1.7976931348623157E+308 },
                    { 7, 10000.0, 5.0, 2, 199999.0 },
                    { 8, 200000.0, 17.5, 2, 1.7976931348623157E+308 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PostalCodes");

            migrationBuilder.DropTable(
                name: "RateRanges");

            migrationBuilder.DropTable(
                name: "Taxes");
        }
    }
}
