﻿using Payroll.Domain.BaseStructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Payroll.Domain.BaseStructure.Implementations
{
    public class ResponseMessage
    {
        public object Data { get; protected set; }
        public string Message { get; protected set; }
        public bool IsSuccess
        {
            get
            {
                return string.IsNullOrEmpty(Message);
            }
        }

        protected ResponseMessage(string message)
        {
            Message = message;
        }

        protected ResponseMessage(object data, string message)
        {
            Data = data;
            Message = message;
        }

        protected ResponseMessage(IEnumerable<object> data, string message)
        {
            Data = data;
            Message = message;
        }

        public static ResponseMessage Ok()
        {
            return new ResponseMessage(string.Empty);
        }

        public static ResponseMessage Ok(object data)
        {
            return new ResponseMessage(data, string.Empty);
        }

        public static ResponseMessage Ok(IEnumerable<object> data)
        {
            return new ResponseMessage(data, string.Empty);
        }

        public static ResponseMessage Ok<TViewModelType>(IEntity data) where TViewModelType : IViewModel
        {
            var viewModel = data?.ToViewModel<TViewModelType>();

            return new ResponseMessage(viewModel, string.Empty);
        }

        public static ResponseMessage Ok<TViewModelType>(IEnumerable<IEntity> data) where TViewModelType : IViewModel
        {
            var viewModelList = data?
                .ToList()
                .ConvertAll(entity => entity.ToViewModel<TViewModelType>());


            return new ResponseMessage(viewModelList, string.Empty);
        }


        public static async Task<ResponseMessage> BadRequest(IEntity entity)
        {
            var message = await entity.GetErrorMessage();

            return new ResponseMessage(message);
        }

        public static ResponseMessage BadRequest(string message)
        {
            return new ResponseMessage(message);
        }

        public static ResponseMessage BadRequest(IEnumerable<string> errors)
        {
            var message = string.Join(Environment.NewLine, errors) ?? string.Empty;

            return new ResponseMessage(message);
        }

    }
}
