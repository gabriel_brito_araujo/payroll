﻿using FluentValidation;
using System;
using System.Linq;
using System.Threading.Tasks;
using Payroll.Domain.BaseStructure.Interfaces;

namespace Payroll.Domain.BaseStructure.Implementations
{
    public abstract class Entity<T> : IEntity
    {
        public IValidator Validator { get; set; }

        protected Entity() { }

        public T Id { get; protected set; }

        public bool IsNew
        {
            get
            {
                return Id.Equals(default(T));
            }
        }

        public virtual IViewModel ToViewModel<TViewModel>() where TViewModel : IViewModel
        {
            var type = typeof(TViewModel);

            var viewModel = (TViewModel)Activator.CreateInstance(type);

            return viewModel.ConvertFromEntity(this);
        }


        public virtual async Task<bool> HasValidState()
        {
            var validationResult = await Validator?.ValidateAsync(this);

            return validationResult?.IsValid ?? true;
        }

        public virtual async Task<string> GetErrorMessage()
        {
            var validationResult = await Validator?.ValidateAsync(this);

            var errors = validationResult?.Errors.Select(e => e.ErrorMessage);

            return string.Join(Environment.NewLine, errors) ?? string.Empty;
        }
    }

}
