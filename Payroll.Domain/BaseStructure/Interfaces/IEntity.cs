﻿using System.Threading.Tasks;

namespace Payroll.Domain.BaseStructure.Interfaces
{
    public interface IEntity
    {
        Task<bool> HasValidState();
        Task<string> GetErrorMessage();
        IViewModel ToViewModel<TViewModel>() where TViewModel : IViewModel;
    }
}
