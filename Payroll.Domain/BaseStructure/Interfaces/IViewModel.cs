﻿
namespace Payroll.Domain.BaseStructure.Interfaces
{
    public interface IViewModel
    {
        IViewModel ConvertFromEntity(IEntity entity);
    }
}
