﻿using Payroll.Domain.BaseStructure.Implementations;
using Payroll.Domain.PostalCodeDomain.Entities.Validators;
using Payroll.Domain.TaxDomain.Enums;

namespace Payroll.Domain.PostalCodeDomain.Entities
{
    public class PostalCode : Entity<int?>
    {
        public string Code { get; protected set; }
        public TaxType TaxType { get; protected set; }

        public PostalCode()
        {

        }

        protected PostalCode(int id, string code, TaxType taxType)
        {
            Id = id;
            Code = code;
            TaxType = taxType;
        }

        protected PostalCode(string code, TaxType taxType)
        {
            Code = code;
            TaxType = taxType;
        }

        public static PostalCode Create(string code, TaxType taxType)
        {
            return new PostalCode(code, taxType)
            {
                Validator = new PostalCodeValidator()
            };
        }

        public static PostalCode Load(int id, string code, TaxType taxType)
        {
            return new PostalCode(id, code, taxType)
            {
                Validator = new PostalCodeValidator()
            };
        }
    }
}
