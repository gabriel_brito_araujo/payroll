﻿using FluentValidation;

namespace Payroll.Domain.PostalCodeDomain.Entities.Validators
{
    public class PostalCodeValidator : AbstractValidator<PostalCode>
    {
        public PostalCodeValidator()
        {
            RuleFor(x => x.Code)
                .NotEmpty()
                .WithMessage("Postal Code must be informed.");

            RuleFor(x => x.TaxType)
                .NotEmpty()
                .WithMessage("Postal Code's tax must be informed.");
        }
    }
}
