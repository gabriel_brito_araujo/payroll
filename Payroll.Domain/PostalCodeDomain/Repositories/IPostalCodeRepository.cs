﻿using Payroll.Domain.PostalCodeDomain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Payroll.Domain.PostalCodeDomain.Repositories
{
    public interface IPostalCodeRepository
    {
        Task<IEnumerable<PostalCode>> Get();
        Task<PostalCode> Get(string postalCode);
    }
}
