﻿using Payroll.Domain.RateRangeDomain.Entities;
using System.Collections.Generic;

namespace Payroll.Domain.TaxDomain.Entities
{
    public class ProgressiveTax : Tax
    {
        protected ProgressiveTax()
        {

        }
        protected ProgressiveTax(double amount)
        {
            Amount = amount;
        }

        protected ProgressiveTax(int id, double amount, double taxAmount)
        {
            Id = id;
            Amount = amount;
            TaxAmount = taxAmount;
        }

        public override double TaxCalculation(IEnumerable<RateRange> ranges)
        {
            var amountTemp = Amount;

            foreach (var range in ranges)
            {
                if (amountTemp > 0)
                {
                    if (amountTemp > range.To)
                    {
                        TaxAmount += range.To * (range.Rate / 100);
                    }
                    else
                    {
                        TaxAmount += amountTemp * (range.Rate / 100);
                    }

                    amountTemp -= range.To;
                }
            }

            return TaxAmount;
        }

        public static ProgressiveTax Create(double amount)
        {
            return new ProgressiveTax(amount);
        }

        public static ProgressiveTax Load(int id, double amount, double taxAmount)
        {
            return new ProgressiveTax(id, amount, taxAmount);
        }
    }
}
