﻿using FluentValidation;

namespace Payroll.Domain.TaxDomain.Entities.Validators
{
    public class TaxValidator: AbstractValidator<Tax>
    {
        public TaxValidator()
        {
            RuleFor(x => x.Amount)
                .NotEmpty()
                .WithMessage("Amount must be informed.");

        }
    }
}
