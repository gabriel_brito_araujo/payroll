﻿using Payroll.Domain.RateRangeDomain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Payroll.Domain.TaxDomain.Entities
{
    public class FlatTax : Tax
    {
        protected FlatTax()
        {

        }

        protected FlatTax(double amount)
        {
            Amount = amount;
        }

        protected FlatTax(int id, double amount, double taxAmount)
        {
            Id = id;
            Amount = amount;
            TaxAmount = taxAmount;
        }


        public override double TaxCalculation(IEnumerable<RateRange> ranges)
        {
            var rateRange = ranges.LastOrDefault();

            TaxAmount = Amount * (rateRange.Rate / 100);

            return TaxAmount;
        }


        public static FlatTax Create(double amount)
        {
            return new FlatTax(amount);
        }

        public static FlatTax Load(int id, double amount, double taxAmount)
        {
            return new FlatTax(id, amount, taxAmount);
        }
    }
}
