﻿
using Payroll.Domain.BaseStructure.Implementations;
using Payroll.Domain.RateRangeDomain.Entities;
using Payroll.Domain.TaxDomain.Entities.Validators;
using System;
using System.Collections.Generic;

namespace Payroll.Domain.TaxDomain.Entities
{
    public abstract class Tax : Entity<int?>
    {
        protected Tax()
        {
            Validator = new TaxValidator();
        }

        public double Amount { get; protected set; }

        public double TaxAmount { get; protected set; }

        public DateTime CreatedAt
        {
            get
            {
                return DateTime.UtcNow;
            }
        }

        public abstract double TaxCalculation(IEnumerable<RateRange> ranges);
    }
}
