﻿using Payroll.Domain.BaseStructure.Interfaces;
using Payroll.Domain.TaxDomain.Entities;

namespace Payroll.Domain.TaxDomain.ViewModels
{
    public class TaxVm : IViewModel
    {
        public string PostalCode { get; set; }
        public double Amount { get; set; }

        public IViewModel ConvertFromEntity(IEntity entity)
        {
            if (!(entity is Tax tax))
                return this;

            Amount = tax.Amount;


            return this;
        }
    }
}
