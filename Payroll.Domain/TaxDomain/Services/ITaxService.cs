﻿using System.Threading.Tasks;
using Payroll.Domain.BaseStructure.Implementations;

namespace Payroll.Domain.TaxDomain.Services
{
    public interface ITaxService
    {
        Task<ResponseMessage> GetTax(string code, double amount);
    }
}
