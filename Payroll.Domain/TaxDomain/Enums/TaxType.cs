﻿namespace Payroll.Domain.TaxDomain.Enums
{
    public enum TaxType
    {    
        Progressive = 1,
        Flat = 2
    }
}
