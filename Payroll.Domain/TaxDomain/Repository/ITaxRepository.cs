﻿using Payroll.Domain.TaxDomain.Entities;
using System.Threading.Tasks;

namespace Payroll.Domain.TaxDomain.Repository
{
    public interface ITaxRepository
    {
        Task Store(Tax tax);
    }
}
