﻿using Payroll.Domain.TaxDomain.Entities;
using Payroll.Domain.TaxDomain.Enums;

namespace Payroll.Domain.TaxDomain.Factories
{
    public class TaxFactory
    {
        public static Tax Create(TaxType taxType, double amount)
        {
            switch (taxType)
            {
                case TaxType.Flat:
                    return FlatTax.Create(amount);
                default:
                    return ProgressiveTax.Create(amount);
            }
        }
    }
}
