﻿using Payroll.Domain.RateRangeDomain.Entities;
using Payroll.Domain.TaxDomain.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Payroll.Domain.RateRangeDomain.Repositories
{
    public interface IRateRangeRepository
    {
        Task<IEnumerable<RateRange>> Get(double amount, TaxType taxType);
    }
}
