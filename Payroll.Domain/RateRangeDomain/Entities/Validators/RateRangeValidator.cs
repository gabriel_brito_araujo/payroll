﻿using FluentValidation;

namespace Payroll.Domain.RateRangeDomain.Entities.Validators
{
    public class RateRangeValidator : AbstractValidator<RateRange>
    {
        public RateRangeValidator()
        {
            RuleFor(x => x.Rate)
                .NotEmpty()
                .WithMessage("Rate must be informed.");

            RuleFor(x => x.To)
                .GreaterThan(x => x.From)
                .WithMessage("Range To must be greater than From.");

            RuleFor(x => x.TaxType)
            .NotEmpty()
            .WithMessage("Tax type must be informed.");
        }
    }
}
