﻿using Payroll.Domain.BaseStructure.Implementations;
using Payroll.Domain.RateRangeDomain.Entities.Validators;
using Payroll.Domain.TaxDomain.Enums;

namespace Payroll.Domain.RateRangeDomain.Entities
{
    public class RateRange : Entity<int?>
    {
        protected RateRange()
        {

        }

        protected RateRange(double rate, double from, double to, TaxType taxType)
        {
            Rate = rate;
            From = from;
            To = to;
            TaxType = taxType;
        }

        protected RateRange(int id, double rate, double from, double to, TaxType taxType)
        {
            Id = id;
            Rate = rate;
            From = from;
            To = to;
            TaxType = taxType;
        }

        public double Rate { get; protected set; }
        public double From { get; protected set; }
        public double To { get; protected set; }
        public TaxType TaxType { get; protected set; }

        public static RateRange Load(int id, double rate, double from, double to, TaxType taxType)
        {
            return new RateRange(id, rate, from, to, taxType)
            {
                Validator = new RateRangeValidator()
            };
        }

        public static RateRange Create(double rate, double from, double to, TaxType taxType)
        {
            return new RateRange(rate, from, to,taxType)
            {
                Validator = new RateRangeValidator()
            };
        }

    }
}
