﻿using System.Threading.Tasks;
using Payroll.Domain.BaseStructure.Implementations;
using Payroll.Domain.PostalCodeDomain.Repositories;
using Payroll.Domain.RateRangeDomain.Repositories;
using Payroll.Domain.TaxDomain.Factories;
using Payroll.Domain.TaxDomain.Repository;
using Payroll.Domain.TaxDomain.Services;

namespace Payroll.Services
{
    public class TaxService : ITaxService
    {
        private readonly IRateRangeRepository _rateRangeRepository;
        private readonly IPostalCodeRepository _postalCodeRepository;
        private readonly ITaxRepository _taxRepository;

        public TaxService(IPostalCodeRepository postalCodeRepository, IRateRangeRepository rateRangeRepository,
            ITaxRepository taxRepository)
        {
            _rateRangeRepository = rateRangeRepository;
            _postalCodeRepository = postalCodeRepository;
            _taxRepository = taxRepository;
        }


        public async Task<ResponseMessage> GetTax(string code, double amount)
        {
            if (string.IsNullOrEmpty(code))
                return ResponseMessage.BadRequest("Postal Code is required");

            var postalCode = await _postalCodeRepository.Get(code);

            if (postalCode == null)
                return ResponseMessage.BadRequest("Postal Code was not found");

            var tax = TaxFactory.Create(postalCode.TaxType, amount);

            if (!await tax.HasValidState())
                return await ResponseMessage.BadRequest(tax);

            var ranges = await _rateRangeRepository.Get(tax.Amount, postalCode.TaxType);

            var taxCalculated = tax.TaxCalculation(ranges);

            await _taxRepository.Store(tax).ConfigureAwait(false);

            return ResponseMessage.Ok(taxCalculated);
        }
    }
}
